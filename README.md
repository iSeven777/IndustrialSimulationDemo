# IndustrialSimulationDemo
工业仿真项目功能探索

### 使用说明

### 1. 模型导入导出工具
    示例场景 - ExportToolDemo，通过菜单 Tool/Mesh/.. 调用。使用 Export 包里的代码，不需要挂载，直接使用静态方法

### 2. 相机聚焦工具
    示例场景 - FocusingToolDemo，需要挂载，需要导入 Cinemachine 包

### 3. 选择包围盒工具
    示例场景 - GetBoundsToolDemo，直接挂载即可使用，需要提供想选择的物体的列表

### 4. 文字和箭头
    示例场景 - TextAndArrowsToolDemo，挂载并提供参数即可使用