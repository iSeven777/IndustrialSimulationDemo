﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Text;

public class ObjExporterScript
{
    private static int StartIndex = 0;

    public static void Start()
    {
        StartIndex = 0;
    }
    public static void End()
    {
        StartIndex = 0;
    }

    public static string MeshToString(MeshFilter mf, Transform t, bool uselhcoords = true, bool separateSubmeshes = true)
    {


        int numVertices = 0;
        Mesh m = mf.sharedMesh;
        if (!m)
        {
            return "####Error####";
        }
        Material[] mats = mf.GetComponent<Renderer>().sharedMaterials;

        StringBuilder sb = new StringBuilder();
        
       

        foreach (Vector3 vv in m.vertices)
        {
            Vector3 v;
            if (!separateSubmeshes)
                v = t.TransformPoint(vv);
            else
                v = vv;
            numVertices++;
            sb.Append(string.Format("v {0} {1} {2}\n",(uselhcoords ? v.x : -v.x), v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 nn in m.normals)
        {
            //Vector3 v = r * nn; 不知道是什么，下面是为了不出错
            Vector3 v = nn;
            sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
        }
        sb.Append("\n");
        foreach (Vector3 v in m.uv)
        {
            sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
        }
        for (int material = 0; material < m.subMeshCount; material++)
        {
            sb.Append("\n");
            sb.Append("usemtl ").Append(mats[material].name).Append("\n");
            sb.Append("usemap ").Append(mats[material].name).Append("\n");

            int[] triangles = m.GetTriangles(material);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
                    triangles[(uselhcoords ? i : i + 1)] + 1 + StartIndex, triangles[(uselhcoords ? i + 1 : i)] + 1 + StartIndex, triangles[i + 2] + 1 + StartIndex));
            }
        }

        // 加入这一句，会将所有网格整合成一个网格
        //StartIndex += numVertices;

        return sb.ToString();
    }
}

public class ObjExporter : ScriptableObject
{
    public static void DoExport(string meshName,string fileName,bool separateSubmeshes = true)
    {
        
        ObjExporterScript.Start();

        StringBuilder meshString = new StringBuilder();

        meshString.Append("#" + meshName + ".obj"
                            + "\n#" + System.DateTime.Now.ToLongDateString()
                            + "\n#" + System.DateTime.Now.ToLongTimeString()
                            + "\n#-------"
                            + "\n\n");

        Transform t = Selection.gameObjects[0].transform;

        Vector3 originalPosition = t.position;
        t.position = Vector3.zero;

        meshString.Append("o ").Append(t.name).Append("\n");


        meshString.Append(processTransform(t, separateSubmeshes));

        WriteToFile(meshString.ToString(), fileName);

        t.position = originalPosition;

        ObjExporterScript.End();
        Debug.Log("Exported Mesh: " + fileName);
    }

    static string processTransform(Transform t, bool separateSubmeshes = true)
    {
        StringBuilder meshString = new StringBuilder();

        meshString.Append("\n#" + t.name
                        + "\n#-------"
                        + "\n");

        MeshFilter mf = t.GetComponent<MeshFilter>();

        if (mf)
        {
            meshString.Append(ObjExporterScript.MeshToString(mf, t));
        }
        
        for (int i = 0; i < t.childCount; i++)
        {
            meshString.Append(processTransform(t.GetChild(i), separateSubmeshes));

            if (separateSubmeshes)
            {
                Vector3 s = t.GetChild(i).localScale;
                Vector3 p = t.GetChild(i).localPosition;
                Quaternion r = t.GetChild(i).localRotation;

                meshString.Append("\n# Transform\n");
                meshString.Append(string.Format("p {0} {1} {2}\n", p.x, p.y, p.z));
                meshString.Append(string.Format("r {0} {1} {2} {3}\n", r.x, r.y, r.z, r.w));
                meshString.Append(string.Format("s {0} {1} {2}\n", s.x, s.y, s.z));
                if (t.GetChild(i).transform.parent != null)
                {
                    meshString.Append(string.Format("parent {0}\n", t.GetChild(i).parent.name));
                }
                meshString.Append("\n");
            }

            meshString.Append("g ").Append(t.GetChild(i).name).Append("\n");
        }

        return meshString.ToString();
    }

    static void WriteToFile(string s, string filename)
    {
        using (StreamWriter sw = new StreamWriter(filename))
        {
            sw.Write(s);
        }
    }
}