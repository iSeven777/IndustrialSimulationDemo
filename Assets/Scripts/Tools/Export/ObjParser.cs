﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ObjParser
{
    /// <summary>
    /// 将 obj 文件解析成 游戏物体（支持子物体）
    /// </summary>
    /// <param name="url">obj文件路径</param>
    /// <returns>游戏对象</returns>
    public static GameObject ParseToGameObject(string url)
    {
        GameObject root = new GameObject();
        GameObject gameObject;
        Transform parent = null;
        Vector3 position = Vector3.zero;
        Quaternion rotation = new Quaternion(0,0,0,1);
        Vector3 scale = new Vector3(1,1,1);
        Mesh mesh = new Mesh();
        string mat = "";

        List<Vector3> vts = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<int> tris = new List<int>();
        
        StreamReader sr = new StreamReader(url);

        while (!sr.EndOfStream)
        {
            string line = sr.ReadLine();
            string[] split = line.Split(' ');

            //  p(position) ,s(scale) ,r(rotation) ,parent 标签是一般 obj 文件所没有的
            switch (split[0])
            {
                case "o":
                    {
                        root.name = split[1];
                    }
                    break;

                case "g":
                    {
                        // 出现一个 g 代表解析了一个游戏物体
                        
                            gameObject = new GameObject();
                            gameObject.transform.parent = root.transform;

                            if(vts.Count > 0)
                            {
                                gameObject.AddComponent<MeshFilter>();
                                gameObject.AddComponent<MeshRenderer>();
                                
                                mesh.name = split[1];
                                mesh.vertices = vts.ToArray();
                                mesh.uv = uvs.ToArray();
                                mesh.uv2 = uvs.ToArray();
                                mesh.normals = normals.ToArray();
                                mesh.triangles = tris.ToArray();

                                mesh.RecalculateBounds();
                                mesh.RecalculateNormals();
                                mesh.RecalculateTangents();

                                gameObject.GetComponent<MeshFilter>().mesh = mesh;
                                // TODO: 找不到根据 mat 文件创建材质的方法，材质都是 default-material
                                gameObject.GetComponent<MeshRenderer>().materials[0] = new Material(mat);
                            }

                            gameObject.name = split[1];
                            gameObject.transform.localPosition = position;
                            gameObject.transform.localRotation = rotation;
                            gameObject.transform.localScale = scale;

                            if (parent != null)
                            {
                                gameObject.transform.parent = parent;
                                parent = null;
                            }

                            mesh = new Mesh();
                            vts.Clear();
                            uvs.Clear();
                            normals.Clear();
                            tris.Clear();
                        
                    }
                    break;
                case "usemtl":
                    {
                        mat = split[1];
                    }
                    break;
                case "p":
                    {
                        position = new Vector3(float.Parse(split[1]), float.Parse(split[2]), float.Parse(split[3]));
                    }
                    break;
                case "r":
                    {
                        rotation = new Quaternion(float.Parse(split[1]), float.Parse(split[2]), float.Parse(split[3]), float.Parse(split[4]));
                    }
                    break;
                case "s":
                    {
                        scale = new Vector3(float.Parse(split[1]), float.Parse(split[2]), float.Parse(split[3]));
                    }
                    break;
                case "parent":
                    {
                        parent = root.transform.Find(split[1]);
                    }
                    break;
                case "v":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);
                        float z = float.Parse(split[3]);

                        vts.Add(new Vector3(x, y, z));
                    }
                    break;
                case "vn":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);
                        float z = float.Parse(split[3]);

                        normals.Add(new Vector3( x, y, z));
                    }
                    break;

                case "vt":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);

                        uvs.Add(new Vector2(x, y));
                    }
                    break;

                case "f":
                    {
                        for (int i = 1; i < split.Length; ++i)
                        {
                            string[] sp = split[i].Split('/');
                            tris.Add(int.Parse(sp[0]) - 1);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return root;
    }

    /// <summary>
    /// 将 obj 文件解析成 mesh
    /// </summary>
    /// <param name="url">obj文件路径</param>
    /// <returns>unity mesh</returns>
    public static Mesh ParseToMesh(string url)
    {
        Mesh mesh = new Mesh();

        List<Vector3> vts = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<int> tris = new List<int>();


        StreamReader sr = new StreamReader(url);

        while (!sr.EndOfStream)
        {
            string line = sr.ReadLine();
            string[] split = line.Split(' ');

            switch (split[0])
            {
                case "o":
                    mesh.name = split[1];
                    break;

                case "v":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);
                        float z = float.Parse(split[3]);

                        vts.Add(new Vector3(x, y, z));
                    }
                    break;
                case "vn":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);
                        float z = float.Parse(split[3]);

                        normals.Add(new Vector3(x, y, z));
                    }
                    break;

                case "vt":
                    {
                        float x = float.Parse(split[1]);
                        float y = float.Parse(split[2]);

                        uvs.Add(new Vector2(x, y));
                    }
                    break;

                case "f":
                    {
                        for (int i = 1; i < split.Length; ++i)
                        {
                            string[] sp = split[i].Split('/');
                            tris.Add(int.Parse(sp[0]) - 1);
                        }
                    }
                    break;

                default:

                    break;
            }
        }

        mesh.vertices = vts.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = tris.ToArray();

        return mesh;
    }
}
