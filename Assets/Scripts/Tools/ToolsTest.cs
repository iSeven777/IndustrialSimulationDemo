﻿using UnityEditor;
using UnityEngine;
using MeshTK;

public class ToolsTest : MonoBehaviour
{

    /// <summary>
    /// 重新计算包围物体的 boxCollider
    /// </summary>
    [MenuItem("Tool/CaculateBoundCollider")]
    static void calculateBoundCollider()
    {
        CalculateBoundCollider.calculateBoundCollider(Selection.activeGameObject.transform);
    }

    /// <summary>
    /// 重置模型的中心点位置
    /// </summary>
    [MenuItem("Tool/ResetCenterPosition")]
    static void resetCenterPosition()
    {
        ResetCenterPosition.resetCenterPosition(Selection.activeTransform);
    }

    /// <summary>
    /// 重置模型的中心点位置（包括MeshFilter）
    /// </summary>
    [MenuItem("Tool/ResetCenterPositionWithMeshFilter")]
    static void resetCenterPositionWithMeshFilter()
    {
        ResetCenterPosition.resetCenterPositionWithMeshFilter(Selection.activeTransform);
    }

    /// <summary>
    /// 网格导入
    /// </summary>
    [MenuItem("Tool/Mesh/Import/Wavefront OBJ")]
    static void DoimportObj()
    {
        string objFileName = EditorUtility.OpenFilePanel("Import .obj file", "", "obj");
        GameObject game = ObjParser.ParseToGameObject(objFileName);
    }

    /// <summary>
    /// 网格导出（带子物体）
    /// </summary>
    [MenuItem("Tool/Mesh/Export/Wavefront OBJ")]
    static void DoExportWSubmeshes()
    {
        if (Selection.gameObjects.Length == 0)
        {
            Debug.Log("Didn't Export Any Meshes; Nothing was selected!");
            return;
        }

        string meshName = Selection.gameObjects[0].name;
        string fileName = EditorUtility.SaveFilePanel("Export .obj file", "", meshName, "obj");
        ObjExporter.DoExport(meshName,fileName, true);
    }

    /// <summary>
    /// 网格导出（不带子物体）
    /// </summary>
    [MenuItem("Tool/Mesh/Export/Wavefront OBJ (No Submeshes)")]
    static void DoExportWOSubmeshes()
    {
        if (Selection.gameObjects.Length == 0)
        {
            Debug.Log("Didn't Export Any Meshes; Nothing was selected!");
            return;
        }

        string meshName = Selection.gameObjects[0].name;
        string fileName = EditorUtility.SaveFilePanel("Export .obj file", "", meshName, "obj");
        ObjExporter.DoExport(meshName,fileName,false);
    }
}
