﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Focusing : MonoBehaviour {

    public GameObject cinemachine;
    public GameObject mainCamera;
    public GameObject newCamera;

    private List<Transform> hitAllChildren=new List<Transform>();
	
	void Update () {
        //MobilePick();
        newCamera.transform.position = cinemachine.transform.position;
        newCamera.transform.rotation = cinemachine.transform.rotation;//摄像机跟随
        MousePick();
	}

    /// <summary>
    /// 移动端点击聚焦
    /// </summary>
    void MobilePick()
    {
        if (Input.touchCount != 1)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.name);
                //Debug.Log(hit.transform.tag);
            }
        }
    }

    /// <summary>
    /// 使用鼠标点击聚焦
    /// </summary>
    void MousePick()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                hitAllChildren.Add(hit.collider.gameObject.transform);//方便转回default
                hit.collider.gameObject.layer = 8;//hit对象转到focus层
                foreach (Transform child in hit.collider.gameObject.transform)
                {
                    hitAllChildren.Add(child);
                    child.gameObject.layer = 8;
                }//遍历hit对象下的所有子对象，添加到list，同时转到focus层
                mainCamera.GetComponent<AudioListener>().enabled = false;
                newCamera.SetActive(true);
                cinemachine.SetActive(true);
                cinemachine.GetComponent<CinemachineVirtualCamera>().Follow = hit.transform;
                cinemachine.GetComponent<CinemachineVirtualCamera>().LookAt = hit.transform;//插件cinemachine
            }
            else//复原
            {
                cinemachine.SetActive(false);
                newCamera.SetActive(false);
                mainCamera.GetComponent<AudioListener>().enabled = true;
                foreach (Transform child in hitAllChildren)
                {
                    child.gameObject.layer = 0;
                }//遍历list将对象转回default层
                hitAllChildren.Clear();//list清空
            }
        }
    }
}
