﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAndArrows : MonoBehaviour
{
    public LineRenderer arrowheadLineRenderer1;
    public LineRenderer arrowheadLineRenderer2;
    public TextMesh textmesh;
    public GameObject text;
    public Camera maincamera;

    private List<Vector3> LineVertices = new List<Vector3> ();
    private LineRenderer lineRenderer;
    private float distance=0;
    private bool isNothing = false;
    private float angle1;
    private float angle2;
    private float angle3;

    void Start()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.name == "Plane")
                {
                    Vector3 wordHitPoint = hit.point;
                    Vector3 localHitPoint = transform.InverseTransformPoint(wordHitPoint);
                    LineVertices.Add(localHitPoint);
                    //GameObject hitGB = hit.collider.gameObject;
                    //hitGB.AddComponent<GUIText>();
                }
                else
                {
                    //Debug.Log("nothing");
                    isNothing = true;
                }
            }

        }
         if (Input.GetMouseButtonDown(0) && LineVertices.Count== 2)
         {
            lineRenderer.enabled = true;
            arrowheadLineRenderer1.enabled = true;
            arrowheadLineRenderer2.enabled = true;
            text.SetActive(true);
            DrawLine();
            CalculateDistance();
            DrawArrowhead();
        }
        if ((Input.GetMouseButtonDown(0) && LineVertices.Count == 3)||(Input.GetMouseButtonDown(0) && isNothing==true))
        {
          //  Debug.Log("3||noting");
            isNothing = false;
            LineVertices.Clear();
            lineRenderer.enabled = false;
            arrowheadLineRenderer1.enabled = false;
            arrowheadLineRenderer2.enabled = false;
            text.SetActive(false);
            RotateRecover();
        }

    }

    void OnGUI()
    {
        if (LineVertices.Count == 0) return;

        GUI.color = Color.red;

        for (int i = 0; i < LineVertices.Count; i++)
        {
            Vector3 worldPoint = transform.TransformPoint(LineVertices[i]);
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(worldPoint);
            Vector3 uiPoint = new Vector3(screenPoint.x, Camera.main.pixelHeight - screenPoint.y, screenPoint.z);
            GUI.Label(new Rect(uiPoint, new Vector2(50, 50)),i.ToString());
        }

            //if (LineVertices.Count==2)
            //{
            //    GUI.color = Color.black;
            //    Vector3 worldPoint2 = transform.TransformPoint((LineVertices[0] + LineVertices[1]) / 2);
            //    Vector3 screenPoint2 = Camera.main.WorldToScreenPoint(worldPoint2);
            //    Vector3 uiPoint = new Vector3(screenPoint2.x, Camera.main.pixelHeight - screenPoint2.y, screenPoint2.z);
            //    GUI.Label(new Rect(uiPoint, new Vector2(100, 80)), distance.ToString());
            //}
            //GUI实现文字-会输出在屏幕上

    }

    /// <summary>
    /// 划线
    /// </summary>
    void DrawLine()
    {
        for (int i = 0; i < LineVertices.Count; i++)
        {
            lineRenderer.SetPosition(i, LineVertices[i]);
        }
    }

    /// <summary>
    /// 计算两点距离
    /// </summary>
    void CalculateDistance()
    {
        //计算距离
        distance = (LineVertices[0]- LineVertices[1]).magnitude;
        textmesh.text = distance.ToString()+"m";
        //计算中点
        Vector3 middlePoint;
        middlePoint.x = (LineVertices[1].x + LineVertices[0].x) * 0.5f;
        middlePoint.y = (LineVertices[1].y + LineVertices[0].y) * 0.5f;
        middlePoint.z = (LineVertices[1].z + LineVertices[0].z) * 0.5f;
        textmesh.transform.position = middlePoint;
        //计算夹角
        Vector3 a= LineVertices[1] - LineVertices[0];

        angle1 = Vector2.Angle(new Vector2(a.x,a.z), new Vector3(1, 0,0));
        angle2 = Vector2.Angle(new Vector2(a.y, a.z), new Vector3(0, 1,0));
        angle3 = Vector2.Angle(new Vector2(a.x, a.y), new Vector3(1, 0,0));

        if (LineVertices[0].z > LineVertices[1].z)
        {
            angle1 = -angle1;
            angle2 = -angle2;
        }
        textmesh.transform.rotation = Quaternion.Euler(90,-angle1,-angle3);
        //这个可以实现平面上的字


        //textmesh.transform.rotation = Quaternion.FromToRotation(new Vector3(1, 0, 0), a);
        //通过这个可以跟随移动
     
    }

    /// <summary>
    /// 恢复旋转
    /// </summary>
    void RotateRecover()
    {
        textmesh.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
    }

    /// <summary>
    /// 绘制箭头
    /// </summary>
    void DrawArrowhead()
    {
        //首箭头
        Vector3 arrowhead1point ;
        arrowhead1point.x = (LineVertices[1].x - LineVertices[0].x) * 0.1f + LineVertices[0].x;
        arrowhead1point.y = (LineVertices[1].y - LineVertices[0].y) * 0.1f + LineVertices[0].y;
        arrowhead1point.z = (LineVertices[1].z - LineVertices[0].z) * 0.1f + LineVertices[0].z;
        arrowheadLineRenderer1.SetPosition(0, LineVertices[0]);
        arrowheadLineRenderer1.SetPosition(1, arrowhead1point);
        
        //尾箭头
        Vector3 arrowhead2point;
        arrowhead2point.x = LineVertices[1].x - (LineVertices[1].x - LineVertices[0].x) * 0.1f ;
        arrowhead2point.y = LineVertices[1].y - (LineVertices[1].y - LineVertices[0].y) * 0.1f ;
        arrowhead2point.z = LineVertices[1].z - (LineVertices[1].z - LineVertices[0].z) * 0.1f ;
        arrowheadLineRenderer2.SetPosition(0, arrowhead2point);
        arrowheadLineRenderer2.SetPosition(1, LineVertices[1]);


    }

}
