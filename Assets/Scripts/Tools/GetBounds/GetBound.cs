﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GetBound : MonoBehaviour
{
    //LineRenderer(线渲染器)组件
    private LineRenderer _lineRenderer;

    //要用LineRenderer模拟的包围盒对象
    private Bounds _bounds;

    public List<GameObject> gameObjects;
    public Color boundColor = Color.red;
    public Color rectColor = Color.blue;
    private Vector3 start;
    private Vector3 end;
    private Vector3 center;
    private Vector3 location;
    private Material rectMat;
    private bool drawRectangle = false;




    /// <summary>
    /// 利用LineRenderer组件画出包围盒
    /// </summary>
    /// <param name="gameObject">被选中的游戏物体</param>
    public void ShowBounds(GameObject gameObject)
    {
        //计算包围盒大小
        _bounds = CaculatBounds(gameObject);
        //获取LineRenderer组件
        _lineRenderer = gameObject.GetComponent<LineRenderer>();
        if (_lineRenderer == null)
            _lineRenderer = gameObject.AddComponent<LineRenderer>();
        //绘制包围盒
        DrawBoundsLine(_bounds);
    }



    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CancleBounds(gameObjects);
            drawRectangle = true;
            start = Input.mousePosition;


            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);//鼠标的屏幕坐标转化为一条射线
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var hitObj = hit.collider.gameObject;
                ShowBounds(hitObj);
            }
        }
        if (Input.GetMouseButton(0))
        {
            end = Input.mousePosition;
            //center = new Vector3((start.x + end.x) / 2, (start.y + end.y) / 2, Camera.main.farClipPlane / 2);Camera.main.
            //  Physics.BoxCast(center,,transform.LookAt ()
            //CheckSelection(gameObjects);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            drawRectangle = false;
            CheckSelection(gameObjects);
        }
    }
    void Start()
    {
        rectMat = new Material(Shader.Find("Particles/Additive"));
        rectMat.hideFlags = HideFlags.HideAndDontSave;
        rectMat.shader.hideFlags = HideFlags.HideAndDontSave;
    }

    /// <summary>
    /// 取消对其他物体的包围盒选框
    /// </summary>
    private void CancleBounds(List<GameObject> gameObjects)
    {

        foreach (GameObject go in gameObjects)
        {
            Destroy(go.GetComponent<LineRenderer>());
        }
    }


    /// <summary>
    ///传入包围盒对象坐标给LineRenderer对象，设置绘制顺序
    /// </summary>
    /// <param name="bound"></param>
    private void DrawBoundsLine(Bounds bound)
    {
        //获取bound对象的坐标值
        List<Vector3> corners = new List<Vector3>();
        Vector3 corner = _bounds.extents;
        corners.Add(new Vector3(corner.x, corner.y, corner.z) + _bounds.center);
        corners.Add(new Vector3(-corner.x, corner.y, corner.z) + _bounds.center);
        corners.Add(new Vector3(-corner.x, -corner.y, corner.z) + _bounds.center);
        corners.Add(new Vector3(corner.x, -corner.y, corner.z) + _bounds.center);
        corners.Add(new Vector3(corner.x, corner.y, -corner.z) + _bounds.center);
        corners.Add(new Vector3(-corner.x, corner.y, -corner.z) + _bounds.center);
        corners.Add(new Vector3(-corner.x, -corner.y, -corner.z) + _bounds.center);
        corners.Add(new Vector3(corner.x, -corner.y, -corner.z) + _bounds.center);
        //设置LineRenderer的属性
        _lineRenderer.material = new Material(Shader.Find("Particles/Additive"));//线材质
        _lineRenderer.positionCount = 17;//连线点的个数
        //设置直线颜色
        _lineRenderer.startColor = boundColor;
        _lineRenderer.endColor = boundColor;
        //设置直线宽度
        float width = Mathf.Min(corner.x, corner.y, corner.z) / 30f;
        _lineRenderer.startWidth = width;
        _lineRenderer.endWidth = width;

        _lineRenderer.useWorldSpace = false;

        //绘制点的坐标次序
        _lineRenderer.SetPosition(0, corners[0]);
        _lineRenderer.SetPosition(1, corners[1]);
        _lineRenderer.SetPosition(2, corners[2]);
        _lineRenderer.SetPosition(3, corners[3]);
        _lineRenderer.SetPosition(4, corners[0]);
        _lineRenderer.SetPosition(5, corners[4]);
        _lineRenderer.SetPosition(6, corners[7]);
        _lineRenderer.SetPosition(7, corners[3]);
        _lineRenderer.SetPosition(8, corners[2]);
        _lineRenderer.SetPosition(9, corners[6]);
        _lineRenderer.SetPosition(10, corners[5]);
        _lineRenderer.SetPosition(11, corners[1]);
        _lineRenderer.SetPosition(12, corners[0]);
        _lineRenderer.SetPosition(13, corners[4]);
        _lineRenderer.SetPosition(14, corners[5]);
        _lineRenderer.SetPosition(15, corners[6]);
        _lineRenderer.SetPosition(16, corners[7]);
    }


    /// <summary>
    /// 利用传入gameObject 计算包围盒大小
    /// </summary>
    /// <param name="gameObject">被选中的游戏物体</param>
    /// <returns></returns>
    private Bounds CaculatBounds(GameObject gameObject)
    {
        //将游戏物体原先的transform属性暂存一份
        Transform parent = gameObject.transform;
        Vector3 postion = parent.position;
        Quaternion rotation = parent.rotation;
        Vector3 scale = parent.localScale;

        //初始化父物体的transform属性
        parent.position = Vector3.zero;
        parent.rotation = Quaternion.Euler(Vector3.zero);
        parent.localScale = Vector3.one;


        //将子物体的线框(LineRenderer)移除
        LineRenderer[] lineRenderers = parent.GetComponentsInChildren<LineRenderer>();
        foreach (LineRenderer child in lineRenderers)
        {
            DestroyImmediate(child);
        }

        //将父物体的碰撞器和线框(LineRenderer)移除
        DestroyImmediate(parent.GetComponentInParent<LineRenderer>());


        //计算父物体包围盒的中心
        Vector3 center = Vector3.zero;
        MeshRenderer[] renders = parent.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer child in renders)
        {
            center += child.bounds.center;
        }
        center /= parent.GetComponentsInChildren<MeshRenderer>().Length;

        //形成父物体新的包围盒
        Bounds bounds = new Bounds(center, Vector3.zero);
        foreach (MeshRenderer child in renders)
        {
            bounds.Encapsulate(child.bounds);
        }

        //还原父物体 的transform属性
        parent.position = postion;
        parent.rotation = rotation;
        parent.localScale = scale;
        return bounds;
    }

    void OnPostRender()
    {
        if (drawRectangle)
        {
            GL.PushMatrix();
            rectMat.SetPass(0);
            GL.LoadPixelMatrix();
            GL.Begin(GL.QUADS);
            GL.Color(new Color(rectColor.r, rectColor.g, rectColor.b, 0.1f));
            GL.Vertex3(start.x, start.y, 0);
            GL.Vertex3(end.x, start.y, 0);
            GL.Vertex3(end.x, end.y, 0);
            GL.Vertex3(start.x, end.y, 0);
            GL.End();
            GL.Begin(GL.LINES);
            GL.Color(rectColor);
            GL.Vertex3(start.x, start.y, 0);
            GL.Vertex3(end.x, start.y, 0);
            GL.Vertex3(end.x, start.y, 0);
            GL.Vertex3(end.x, end.y, 0);
            GL.Vertex3(end.x, end.y, 0);
            GL.Vertex3(start.x, end.y, 0);
            GL.Vertex3(start.x, end.y, 0);
            GL.Vertex3(start.x, start.y, 0);
            GL.End();
            GL.PopMatrix();
           
        }
    }

    private void CheckSelection(List<GameObject> gameObjects)
    {
        Vector3 p1 = Vector3.zero;
        Vector3 p2 = Vector3.zero;
        Vector3 _start = (start);
        Vector3 _end = (end);
        if (_start.x > _end.x)
        {//这些判断是用来确保p1的xy坐标小于p2的xy坐标，因为画的框不见得就是左下到右上这个方向的
            p1.x = _end.x;
            p2.x = _start.x;
        }
        else
        {
            p1.x = _start.x;
            p2.x = _end.x;
        }

        if (_start.y > _end.y)
        {
            p1.y = _end.y;
            p2.y = _start.y;
        }
        else
        {
            p1.y = _start.y;
            p2.y = _end.y;
        }
        foreach (GameObject obj in gameObjects)
        {
            location = Camera.main.WorldToScreenPoint(obj.transform.position);
            if ((location.x > p1.x &&location.x < p2.x) && (location.y > p1.y && location.y < p2.y)
            && (location.z > Camera.main.nearClipPlane && location.z < Camera.main.farClipPlane))
            {
                ShowBounds(obj);
                Debug.Log("++");
                Debug.Log("p1:" + p1);
                Debug.Log("p2:" + p2);
                Debug.Log("location:" + location);
                Debug.Log("start:" + start);
                Debug.Log("end:" + end);
            }
            else
            {
                
                Debug.Log("--");
                Debug.Log("p1:"+p1);
                Debug.Log("p2:"+p2);
                Debug.Log("location:"+location);
                Debug.Log("start:" + start);
                Debug.Log("end:" +end);
            }
        }
    }
}
