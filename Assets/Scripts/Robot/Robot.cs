﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{

    public GameObject xAxis;
    public GameObject yAxis;
    public GameObject zAxis;
    public bool isLoop = false;
    public AnimationCurve speedCurve;

    private float originXAxis;
    private float originYAxis;
    private float originZAxis;
    private Vector3 position;
    private List<List<Vector3>> taskList = new List<List<Vector3>>();
    private Queue<List<Vector3>> taskQueue = new Queue<List<Vector3>>();
    private Task taskInstance;

    private void Start ()
    {
        position = Vector3.zero;
        originXAxis = xAxis.transform.position.x;
        originYAxis = yAxis.transform.position.y;
        originZAxis = zAxis.transform.position.z;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            List<Vector3> task = new List<Vector3>();
            task.Add(new Vector3(0, 0, 0));
            task.Add(new Vector3(0.5f, 0, 0));
            task.Add(new Vector3(0.5f, -0.25f, 0f));
            task.Add(new Vector3(0.5f, -0.25f, -0.25f));
            task.Add(new Vector3(0.5f, -0.25f, 0f));
            task.Add(new Vector3(-0.5f, 0, 0));
            task.Add(new Vector3(0, 0, 0));
            AddTaskToQueue(task);
        }
        if(Input.GetMouseButtonDown(1))
        {
            List<Vector3> task = new List<Vector3>();
            task.Add(new Vector3(0, 0, 0));
            task.Add(new Vector3(-0.25f, 0, 0));
            task.Add(new Vector3(-0.25f, 0f, -0.25f));
            AddTaskToQueue(task);
        }
        Debug.Log(taskQueue.Count);
    }

    /// <summary>
    /// 往任务队列增加任务
    /// </summary>
    /// <param name="task">任务点列表</param>
    public void AddTaskToQueue(List<Vector3> task)
    {
        taskQueue.Enqueue(task);
        if (taskInstance == null || taskInstance.Running == false)
        {
            taskInstance = new Task(MoveOnTaskQueue(taskQueue));
        }
    }

    /// <summary>
    /// 按任务列表移动
    /// </summary>
    /// <param name="taskList">任务列表</param>
    /// <param name="loop">是否循环</param>
    /// <returns></returns>
    IEnumerator MoveOnTaskList(List<List<Vector3>> taskList,bool loop)
    {
        do
        {
            foreach (var task in taskList)
            {
                yield return StartCoroutine(MoveOnTask(task));
            }
        }
        while (loop);
    }

    /// <summary>
    /// 执行任务队列
    /// </summary>
    /// <param name="taskQueue">任务队列</param>
    /// <returns></returns>
    IEnumerator MoveOnTaskQueue(Queue<List<Vector3>> taskQueue)
    {
        List<Vector3> task;
        while(taskQueue.Count != 0 && (task = taskQueue.Dequeue()) != null)
                yield return StartCoroutine(MoveOnTask(task));
    }

    /// <summary>
    /// 完成一个任务的移动
    /// </summary>
    /// <param name="task">任务点列表</param>
    /// <returns></returns>
    IEnumerator MoveOnTask(List<Vector3> task)
    {
        task.Add(task[0]);
        yield return StartCoroutine(MoveOnPath(task, false));
    }

    /// <summary>
    /// 移动到一个目标点
    /// </summary>
    /// <param name="targetPos">目标点</param>
    public void MoveTo(Vector3 targetPos)
    {
        StartCoroutine(MoveToPosition(targetPos));
    }

    /// <summary>
    /// 按路径循环移动
    /// </summary>
    /// <param name="loop">是否循环</param>
    /// <returns></returns>
    IEnumerator MoveOnPath(List<Vector3> path,bool loop = false)
    {
        do
        {
            foreach (var point in path)
                yield return StartCoroutine(MoveToPosition(point));
        }
        while (loop);
    }

    /// <summary>
    /// 移动到一个目标点
    /// </summary>
    /// <param name="target">目标点</param>
    /// <returns></returns>
    IEnumerator MoveToPosition(Vector3 target)
    {
        // TODO: 增加范围的控制
        while(position != target)
        {
            position = Vector3.MoveTowards(position, target, speedCurve.Evaluate(Mathf.Clamp01(Mathf.Abs(Vector3.Distance(position ,target)))) * Time.deltaTime);
            xAxis.transform.position = new Vector3(originXAxis + position.x, xAxis.transform.position.y, xAxis.transform.position.z);
            yAxis.transform.position = new Vector3(yAxis.transform.position.x, originYAxis + position.y, yAxis.transform.position.z);
            zAxis.transform.position = new Vector3(zAxis.transform.position.x, zAxis.transform.position.y, originZAxis + position.z);
            yield return 0;
        }
    }
}
