﻿using System.Collections.Generic;
using UnityEngine;

public class Transfer : MonoBehaviour {

    private static float rollerStep = 0.08f; 

    public GameObject board;
    public GameObject roller;
    public GameObject foot;

    public float length = 5f;
    public float width = 0.44f;
    public float height = 1;
    public float inclineAngle = 0;

    private int boardCount;
    private int rollerCount;
    private int footCount;
 
    private List<GameObject> rollersList;
    private List<GameObject> boardsList;
    private List<GameObject> footsList;

	// Use this for initialization
	void Start () {
        rollersList = new List<GameObject>();
        boardsList = new List<GameObject>();
        footsList = new List<GameObject>();

        Mesh mesh = board.GetComponent<Mesh>();

        Instance();
    }

    #region 改变外观

    private void Instance()
    {
        GameObject gameObject;

   
        rollerCount = (int)(length / rollerStep);
        if (rollerCount % 10 == 0)
            boardCount = rollerCount / 10;
        else boardCount = rollerCount / 10 + 1;
        footCount = boardCount/2+1;

        if (boardCount < 1)
        {
            gameObject = Translate(Instantiate(foot), 0, 0, -rollerStep);
            gameObject.transform.parent = this.transform;
            footsList.Add(gameObject);
        }

        for (int i = 0; i<boardCount  ; i++)
        {
            if (i == boardCount - 1)
            {
                
                gameObject = Translate(Instantiate(board), 0, 0, i * -rollerStep * 10 - (10 - rollerCount % 10) * -rollerStep);
                gameObject.transform.parent = this.transform;
                boardsList.Add(gameObject);
            }
            else
            {
                gameObject = Translate(Instantiate(board), 0, 0, i * -rollerStep*10);
                gameObject.transform.parent = this.transform;
                boardsList.Add(gameObject);
            }
            
        }

        for(int i=0;i<footCount ;i++)
        {
            if (i == footCount - 1)
            {
                gameObject = Translate(Instantiate(foot), 0, 0,boardsList[boardCount-1] .transform.position.z+-rollerStep*7);
                gameObject.transform.parent = this.transform;
                boardsList.Add(gameObject);
            }
            else
            {
                gameObject = Translate(Instantiate(foot), 0, 0, 2 * i * -rollerStep * 10);
                gameObject.transform.parent = this.transform;
                footsList.Add(gameObject);
            }
        }
        
        for (int i = 0; i<rollerCount; i++)
        {
            gameObject = Translate(Instantiate(roller), 0, 0, -rollerStep * i);
            //gameObject.transform.localScale = new Vector3(0.5f, 1, 1);
          ///  gameObject.transform.localScale.Scale(new Vector3(0.5f, 1 , 1));
            gameObject.transform.parent = this.transform;
            rollersList.Add(gameObject);
        }
    }

    #endregion

    private GameObject Translate(GameObject gameObject,float x, float y, float z)
    {
        gameObject.transform.Translate(x, y, z);
        return gameObject;
    }

    private GameObject Scale(GameObject gameObject, float x, float y, float z)
    {
        gameObject.transform.localScale.Scale(new Vector3(x, y, z));
        return gameObject;
    }


}
