﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMainScript : MonoBehaviour {

    public GameObject inputField;

	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.name == "Plane")
                {
                    Debug.Log(Input.mousePosition);
                    GameObject newInputField = GameObject.Instantiate(inputField, hit.point, Quaternion.Euler(Vector3.zero)) as GameObject;
                    newInputField.transform.parent = GameObject.Find("PlaneCanvas").transform;
                    newInputField.transform.Rotate(Vector3.right, 90);
                }
                else
                {
                    Debug.Log("not plane");
                }

            }
            else
            {
                Debug.Log("nothing");
            }
            this.enabled = false;
        }
    }
}
