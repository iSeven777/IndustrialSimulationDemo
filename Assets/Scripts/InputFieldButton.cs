﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldButton : MonoBehaviour {

    public GameObject MainScript;

	// Use this for initialization
	void Start () {
        Button btn = this.GetComponent<Button>();
        UIEventListener btnListener = btn.gameObject.AddComponent<UIEventListener>();
        btnListener.OnClick += delegate (GameObject gb)
        {
           // MainScript.SetActive(true);
            MainScript.GetComponent<TextMainScript>().enabled = true;
        };
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
